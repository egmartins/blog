class PostsController < ApplicationController
	before_action :find_post, only: [:show, :edit, :update, :destroy]

	def index
		@posts = Post.all.order('created_at DESC').paginate(page: params[:page], per_page: 2)
	end

	def new
		@post = Post.new
	end

	def create
		@post = Post.new(post_params)
		if @post.save
			redirect_to @post, notice: "Post successfuly created!"
		else
			render 'new', notice: "Unable to save the post"
		end
	end

	def show
	end

	def destroy
		@post.destroy
		redirect_to posts_path
	end

	def edit

	end

	def update
		if @post.update post_params
			redirect_to @post, notice: "Post updated successfuly!"
		else
			render 'show'
		end
	end



	private

	def find_post
		@post = Post.friendly.find(params[:id])
	end

	def post_params
		params.require(:post).permit(:title, :content, :slug)
	end


end
